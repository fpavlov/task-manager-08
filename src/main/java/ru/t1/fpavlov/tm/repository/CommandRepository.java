package ru.t1.fpavlov.tm.repository;

import ru.t1.fpavlov.tm.api.ICommandRepository;
import ru.t1.fpavlov.tm.model.Command;

import static ru.t1.fpavlov.tm.constant.ArgumentConst.*;
import static ru.t1.fpavlov.tm.constant.TerminalConst.*;


/*
 * Created by fpavlov on 04.10.2021.
 */
public class CommandRepository implements ICommandRepository {

    public static Command ABOUT = new Command(
            CMD_ABOUT,
            CMD_SHORT_ABOUT,
            "Display developer info"
    );

    public static Command HELP = new Command(
            CMD_HELP,
            CMD_SHORT_HELP,
            "Display list of terminal command"
    );

    public static Command VERSION = new Command(
            CMD_VERSION,
            CMD_SHORT_VERSION,
            "Display program version"
    );

    public static Command EXIT = new Command(
            CMD_EXIT,
            "Quite"
    );

    public static Command INFO = new Command(
            CMD_INFO,
            CMD_SHORT_INFO,
            "Display available resource of system"
    );

    public static Command ARGUMENTS = new Command(
            CMD_ARGUMENTS,
            CMD_SHORT_ARGUMENTS,
            "Display arguments of running"
    );

    public static Command COMMANDS = new Command(
            CMD_COMMANDS,
            CMD_SHORT_COMMANDS,
            "Display names of available commands"
    );

    private static Command[] TERMINAL_COMMANDS = new Command[]{
            ABOUT, HELP, VERSION, EXIT, INFO, ARGUMENTS, COMMANDS
    };

    @Override
    public Command[] getTerminalCommands() {
        return TERMINAL_COMMANDS;
    }

}
