package ru.t1.fpavlov.tm;

import ru.t1.fpavlov.tm.api.ICommandRepository;
import ru.t1.fpavlov.tm.model.Command;
import ru.t1.fpavlov.tm.repository.CommandRepository;

import java.util.Scanner;

import static ru.t1.fpavlov.tm.constant.TerminalConst.*;
import static ru.t1.fpavlov.tm.constant.ArgumentConst.*;
import static ru.t1.fpavlov.tm.util.FormatUtil.bytesToHumanReadable;

public class Application {

    private static ICommandRepository COMMAND_REPOSITORY = new CommandRepository();

    public static void main(String[] args) {
        run(args);
    }

    private static void displayWelcomeText() {
        System.out.println("**WELCOME TO TASK MANAGER**\n");
    }

    private static void displayHelp() {
        final Command[] commands = COMMAND_REPOSITORY.getTerminalCommands();
        System.out.println("Commands:");

        for (final Command command : commands) {
            System.out.println("\t" + command);
        }
    }

    private static void displayAbout() {
        System.out.println("\tPavlov Philipp");
        System.out.println("\tfpavlov@t1-consulting.ru");
    }

    private static void displayVersion() {
        System.out.println("\t1.8.0");
    }

    private static void run(final String[] args) {
        if (areArgumentsAvailable(args)) {
            listenerArgument(args[0]);
            return;
        }

        interactiveCommandProcessing();
    }

    private static void interactiveCommandProcessing() {
        String param;
        final Scanner scanner = new Scanner(System.in);

        displayWelcomeText();

        while (true) {
            System.out.println("-- Please enter a command --");
            param = scanner.nextLine();
            listenerCommand(param);
        }
    }

    private static void listenerCommand(final String command) {
        switch (command) {
            case CMD_HELP:
                displayHelp();
                break;
            case CMD_VERSION:
                displayVersion();
                break;
            case CMD_ABOUT:
                displayAbout();
                break;
            case CMD_INFO:
                displaySystemInfo();
                break;
            case CMD_COMMANDS:
                displayCommands();
                break;
            case CMD_ARGUMENTS:
                displayArguments();
                break;
            case CMD_EXIT:
                quite();
                break;
            default:
                incorrectCommand();
        }
    }

    private static void listenerArgument(final String argument) {
        switch (argument) {
            case CMD_SHORT_HELP:
                displayHelp();
                break;
            case CMD_SHORT_ABOUT:
                displayAbout();
                break;
            case CMD_SHORT_VERSION:
                displayVersion();
                break;
            case CMD_SHORT_INFO:
                displaySystemInfo();
                break;
            case CMD_SHORT_COMMANDS:
                displayCommands();
                break;
            case CMD_SHORT_ARGUMENTS:
                displayArguments();
                break;
            default:
                incorrectArgument();
        }

        System.exit(0);
    }

    private static void incorrectCommand() {
        System.out.println("\tIncorrect Command");
    }

    private static void incorrectArgument() {
        System.out.println("\tIncorrect Argument");
    }

    private static void quite() {
        System.exit(0);
    }

    private static boolean areArgumentsAvailable(final String[] args) {
        return (args != null && args.length > 0);
    }

    private static void displaySystemInfo() {
        Runtime runtime = Runtime.getRuntime();

        System.out.format(
                "System info:%n" +
                        "\t - Available processors: %s%n" +
                        "\t - Free memory: %s%n" +
                        "\t - Maximum memory: %s%n" +
                        "\t - Total memory: %s%n" +
                        "\t - Used memory: %s%n",
                runtime.availableProcessors(),
                bytesToHumanReadable(runtime.freeMemory()),
                bytesToHumanReadable(runtime.maxMemory()),
                bytesToHumanReadable(runtime.totalMemory()),
                bytesToHumanReadable(runtime.totalMemory() - runtime.freeMemory())
        );
    }

    private static void displayArguments() {
        final Command[] commands = COMMAND_REPOSITORY.getTerminalCommands();
        String argumentName;

        for (final Command command : commands) {
            argumentName = command.getArgument();

            if (argumentName == null || argumentName.isEmpty()) {
                continue;
            }

            System.out.println(argumentName);
        }
    }

    private static void displayCommands() {
        final Command[] commands = COMMAND_REPOSITORY.getTerminalCommands();
        String commandName;

        for (final Command command : commands) {
            commandName = command.getName();

            if (commandName == null || commandName.isEmpty()) {
                continue;
            }

            System.out.println(commandName);
        }
    }

}